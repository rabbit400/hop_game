#pragma once

#include "Packs_concept.h"
#include "Packs_details.h"
#include <type_traits>

namespace Packs
{
	template<class...Ts> struct Pack
	{
		static constexpr size_t Count = sizeof...(Ts);

		using First = details::First_type_t<Ts...>;
		using Last = details::Last_type_t<Ts...>;

		using Remove_first = details::Remove_first_from_pack_t<Packs::Pack<Ts...>>;
		using Remove_last = details::Remove_last_from_pack_t<Packs::Pack<Ts...>>;

		using Reverse = details::Reverse_types_t<Ts...>;

		template <template<class...> class Result> using Apply = Result<Ts...>;

		template <class ... Prefix>				using Prepend = Packs::Pack<Prefix..., Ts...>;
		template <class ... Postfix>			using Append = Packs::Pack<Ts..., Postfix...>;

		template <Pack_concept Other_pack>		using Concat = details::Concat_packs_t<Pack, Other_pack>;

		template <size_t n>						using Take = details::Take_front_of_pack_t<Pack, n>;
		template <size_t n>						using Skip = details::Skip_front_of_pack_t<Pack, n>;
		template <size_t begin, size_t count>	using Subpack = details::Subpack_t<Pack, begin, count>;

		template <template <class> class F>		using Filter = details::Filter_pack_t<F, Pack>;
		template <template <class> class T>		using Transform = details::Transform_pack_t<T, Pack>;

		template <template <class, class> class Less_than> using Sort = details::Sort_pack_t<Less_than, Pack>;

		template<class T> static constexpr bool Contains = details::Pack_contains_v<Pack, T>;
		template<class T> static constexpr size_t Index_of = details::Pack_index_of_v<Pack, T>;
	};

	template<> struct Pack<>
	{
		static constexpr size_t Count = 0;
		// Not applicable: First, Last, Remove_first, Remove_last
		using Reverse = Pack<>;

		template <template <class...> class Result>			using Apply = Result<>;
		template <class ... Prefix>							using Prepend = Packs::Pack<Prefix...>;
		template <class ... Postfix>						using Append = Packs::Pack<Postfix...>;
		template <class Other_pack>							using Concat = Other_pack;
		template <size_t n>									using Take = Packs::Pack<>;
		template <size_t n>									using Skip = Packs::Pack<>;
		template <size_t begin, size_t count>
		requires(begin == 0 && count == 0)			using Subpack = Pack<>;
		template <template <class> class F>					using Filter = Packs::Pack<>;
		template <template <class> class T>					using Transform = Packs::Pack<>;
		template <template <class, class> class Less_than>	using Sort = Packs::Pack<>;

		template<class T> static constexpr bool Contains = false;
		// Not applicable: Index_of
	};

	template <template <class ...> class Result, Pack_concept P>
	using Apply_pack = details::Apply_pack_t<Result, P>;
	template <class Variadic_type>
	using Read_pack = details::Read_pack_t<Variadic_type>;

	template <Pack_concept P, class ... Prefix>
	using Prepend_to_pack = details::Prepend_to_pack_t<P, Prefix...>;
	template <Pack_concept P, class ... Postfix>
	using Append_to_pack = details::Append_to_pack_t<P, Postfix...>;
	template <Pack_concept P, Pack_concept Q>
	using Concat_packs = details::Concat_packs_t<P, Q>;

	template <Pack_concept P, size_t n>
	using Take_front_of_pack = details::Take_front_of_pack_t<P, n>;
	template <Pack_concept P, size_t n>
	using Skip_front_of_pack = details::Skip_front_of_pack_t<P, n>;
	template <Pack_concept P, size_t begin, size_t count>
	using Subpack = details::Subpack_t<P, begin, count>;

	template <template <class> class F, Pack_concept P>
	using Filter_pack = details::Filter_pack_t<F, P>;
	template <template <class> class Transform, Pack_concept P>
	using Transform_pack = details::Transform_pack_t<Transform, P>;

	template <template <class, class> class Less_than, Pack_concept P>
	using Sort_pack = details::Sort_pack_t<Less_than, P>;

	template<Pack_concept P, class T>
	inline constexpr bool Pack_contains = details::Pack_contains_v<P, T>;
	template<Pack_concept P, class T>
	inline constexpr size_t Pack_index_of = details::Pack_index_of_v<P, T>;
}

#include "Matrix.h"

#include <iostream>
#include <cassert>

using namespace hop_game::Matrix;

void hop_game::Matrix::run_simple_matrix_tests()
{
	auto print_mat = []<size_t M, size_t N, class T>(const Matrix<M, N, T> &mat, const char* title = "new mat")
	{
		//if (title)
		//	std::cout << title << std::endl;
		//for (size_t m = 0; m < M; ++m)
		//{
		//	for (size_t n = 0; n < N; ++n)
		//		std::cout << ' ' << mat(m, n);
		//	std::cout << std::endl;
		//}
	};

	constexpr Matrix<2, 2, float> m{
		[](size_t m, size_t n) { return m * 100 + n + 0.42f; }
	};
	constexpr auto v = m.Row(1)[0];
	constexpr auto u = m.Column(0)[1];
	constexpr auto w = m.Row<1>()[0];
	constexpr auto ww = m.Row_at(0)[1];

	static constinit Float2x2 mm{ m };

	mm(0, 1) = 3.6f;

	static constexpr auto owo = []()
	{
		Matrix<2, 2, float> m{};
		m(0, 0) = 01.69f;
		m(0, 1) = 61.69f;
		m(1, 0) = 31.69f;
		m(1, 1) = 901.69f;
		return m;
	} ();

	// weird that this works :D
	// rowo is a pointer to inside owo
	// feels like a compiletime pointer should not work at runtime...
	static constexpr auto rowo = owo.Row(0);
	auto rowo2 = owo.Row(0);

	const auto& mc = m;
	auto rr = mc.Row(1);
	auto& cc = mc.Row(1)[1];

	static constexpr auto I = Matrix<2, 2, float>::Identity();
	auto J = Matrix<2, 2, float>();

	constexpr auto K = Matrix<2, 2, float>{};

	J += I;
	J *= 2.0f;

	auto L = J * K;
	auto asd = J.Row(0) * I.Column(0);

	static_assert(std::is_same_v<
		std::decay_t<decltype(I)>,
		std::decay_t<decltype(J)>
	>);

	static_assert(I(0, 0) == 1.0f);
	static_assert(K(0, 0) == 0.0f);
	static_assert(K(0, 1) == I(1, 0));

	auto x = m.Row(0);
	auto y = I.Row(0);
	auto z = J.Row(0);

	auto wew = z.at<1>();

	for (size_t m = 0; m < 2; ++m)
		for (size_t n = 0; n < 2; ++n)
		{
			assert(I(m, n) == (m == n ? 1.0f : 0.0f));
			assert(I(m, n) == 0.5f * J(m, n));
		}

	Matrix dilly // deduced to 2x3 float, values must all be float
	{
		{ 1.1f, 2.2f, 3.3f},
		{ 2.6f, 3.7f, 4.8f}
	};
	Matrix<2, 3, double> nilly // double specified: values does not need to be double
	{
		{1.1f, 2, 3},
		{ 2, 3, 4}
	};
	Matrix willy = // using Vector constructor with type deduction
	{
		Vector::Vector{5.55, 6.66},
		Vector::Vector{2.22, 6.66},
		Vector::Vector{4.44, 6.66},
		Vector::Vector{4.44, 5.55}
	};

	print_mat(nilly);
	print_mat(dilly);
	print_mat(willy);
}

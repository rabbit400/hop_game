#include "Map.h"

hop_game::Map hop_game::Map::First_map()
{
	return hop_game::Map{
		{{0, 2}}, // spawn
		{
			{{ -3, 2 }}, // blocks
			{{ -1, 0 }},
			{{ 0, 0 }},
			{{ 1, 0 }},
			{{ 2, 0 }},
			{{ 5, 0 }},
			{{ 10, 0 }},
		}
	};
}

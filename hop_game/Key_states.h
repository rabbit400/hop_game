#pragma once
#include <unordered_map>
#include <SFML/Window/Keyboard.hpp>

struct Key_state
{
	bool started_down;
	bool is_down;
	bool was_pressed;
	bool was_released;

	void pressed()
	{
		is_down = true;
		was_pressed = true;
	}
	void released()
	{
		is_down = false;
		was_released = true;
	}
	void step_frame() // wtf is a good function name???
	{
		started_down = is_down;
		was_pressed = false;
		was_released = false;
	}
};

class Key_states
{
	using Map = std::unordered_map<sf::Keyboard::Key, Key_state>;

	Map states;

public:

	void pressed(sf::Keyboard::Key k) { states[k].pressed(); }
	void released(sf::Keyboard::Key k) { states[k].released(); }
	void step_frame() // wtf is a good function name???
	{
		for (auto& [key, state] : states)
			state.step_frame();
	}

	// By returning a const Key_state instead of a Key_state we make it harder
	// for callers to mutate the result. (Mutating the result would likely be a bug.)
	// 
	// An alternative would be to return const Key_state & (a reference).
	// This would be messy if the key is not already in the map.
	// Two messy solutions:
	//   - a static "zero" instance
	//   - add the key to our map ... but then this function is not const
	//     (We could mark the map mutable but thats not thread-safe.)
	const Key_state operator[](sf::Keyboard::Key k) const
	{
		auto found = states.find(k);
		if (found != states.cend())
			return found->second;
		else
			return Key_state{};
	}
};


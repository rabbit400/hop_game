#include "functions.h"
#include <algorithm>

constexpr float hop_game::functions::clamp(float f, float min, float max)
{
    return std::clamp(f, min, max);
}

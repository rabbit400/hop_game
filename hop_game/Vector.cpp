#include "Vector.h"
#include <iostream>


template <size_t N, class T >
void print_vec(hop_game::Vector::Vector<N, T> v, const char* title = "new vec")
{
	//if (title)
	//	std::cout << title << std::endl;
	//for (size_t n = 0; n < N; ++n)
	//	std::cout << ' ' << v[n];
	//std::cout << std::endl;

	//std::cout << "abs squared: " << v.abs_squared()
	//	<< " abs: " << v.abs()
	//	<< " normalized()[0]: " << v.normalized()[0]
	//	<< " normalize(), v[0]: " << (v.normalize(), v[0])
	//	<< std::endl;
};

void hop_game::Vector::run_simple_vector_tests()
{

	constexpr static Vector<1, double> v1{ [](size_t i) { return 3.67; } };
	constexpr static Vector<2, double> v2{ [](size_t i) { return 3.67; } };
	constexpr static Vector<3, double> v3{ [](size_t i) { return 3.67; } };
	constexpr static Vector<4, double> v4{ [](size_t i) { return 3.67; } };

	print_vec(v1, "v1");
	print_vec(v2, "v2");
	print_vec(v3, "v3");
	print_vec(v4, "v4");

	Vector<3, float> v { 1.2f, 3.3f, 4.4f };
	Vector<3, float> vv = { 1.2f, 3.3f, 4.4f };
	Vector<3, float> vvv { { 1, 2.3f, 3 } };
	Vector<3, float> vvvv = { { 1, 2, 3.3f } };

	Vector<1, float> vdf{ { 1 } };
	Vector<1, float> vdff{ 1.0f };

	Vector w = { 1.2f, 3.3f, 4.4f };

	auto halfs = Vector<3, float>::Halfs();
	print_vec(halfs, "halfs");
}

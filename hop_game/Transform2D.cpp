#include "Transform2D.h"

using namespace hop_game;
using namespace hop_game::Angels;

#include "Angels.h"
#include <cassert>
#include <iostream>

using namespace hop_game::Angels;

void Verify(const Float2& result, const Float2& expected)
{
	Float2 delta = result - expected;
	auto diff = delta.abs();
	assert(diff < 1.0e-6);
}

template <size_t M, size_t N, class T>
void verify(const Matrix::Matrix<M, N, T>& x, const Matrix::Matrix<M, N, T>& y)
{
	T diff_sum{};
	x.for_each_index([&](size_t m, size_t n) { diff_sum += abs(x(m, n) - y(m, n)); });
	assert(diff_sum < 1e-6);
}

void verify_inverse(const Transform2D& t)
{
	verify(Float3x3::Identity(), t.m_matrix * t.m_inverse);
	verify(Float3x3::Identity(), t.m_inverse * t.m_matrix);
}

void hop_game::Transform2D::run_silly_transform2d_tests()
{
	assert(1.0f == 1.0f);

	Transform2D rotate90 = Transform2D::Rotate(Degrees{ 90 });
	Transform2D translate_2_3 = Transform2D::Translate({ {2, 3 } });

	Transform2D rotate90_around_1_1 = Transform2D::Rotate(Degrees{ 90 }, { {1, 1} });

	verify_inverse(rotate90);
	verify_inverse(translate_2_3);
	verify_inverse(rotate90_around_1_1);

	verify_inverse(translate_2_3.Then(translate_2_3));


	Verify(rotate90.Transform({ {1, 1} }), { {-1, 1} });
	Verify(rotate90.Inverse({ {-1, 1} }), { {1, 1} });

	Verify(rotate90.Transform({ {1, 2} }), { {-2, 1} });
	Verify(rotate90.Inverse({ {-2, 1} }), { {1, 2} });

	Verify(translate_2_3.Transform({ {1, 1} }), { {3, 4} });
	Verify(translate_2_3.Inverse({ { 1, 1} }), { {-1, -2} });

	verify(Float3x3::Identity(), rotate90.m_inverse * rotate90.m_matrix);
	verify(Float3x3::Identity(), rotate90.m_matrix * rotate90.m_inverse);
	
	//verify(Float3x3::Identity(), rotate90.m_matrix * rotate90.m_matrix);


	Verify(rotate90_around_1_1.Transform({ {1, 1} }), { { 1, 1 } });
	Verify(rotate90_around_1_1.Inverse({ {1, 1} }), { { 1, 1 } });

	Verify(rotate90_around_1_1.Transform({ {2, 1} }), { { 1, 2 } });
	Verify(rotate90_around_1_1.Inverse({ {1, 2} }), { { 2, 1 } });

}



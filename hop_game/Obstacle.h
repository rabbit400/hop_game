#pragma once

#include "Vector.h"
#include <SFML/Graphics.hpp>

namespace hop_game
{
	class Obstacle
	{

	public:
		Float2 Top_left;
		Float2 Width_height;
		sf::Color Colour;

		void Draw(sf::RenderWindow& window, float bunny_size) const
		{
			auto & v =  window.getDefaultView();
			auto & p = v.getViewport();

			sf::RectangleShape r;
			r.setPosition(Top_left.To_sf(bunny_size));
			r.setSize(Width_height.To_sf(bunny_size));
			r.setFillColor(Colour);
			window.draw(r);
		}
	};
}

#pragma once
#include <Entities_for_systems.h>
#include <SFML/Graphics.hpp>

namespace hop_game
{
	struct IGame
	{
		virtual ~IGame() {};
		virtual void Init_game(hope::Entities_for_systems& man) = 0;
		virtual void On_event(const sf::Event& event) = 0;
		virtual void Run_all_systems_for_frame(hope::Entities_for_systems& man, sf::RenderTarget& render_target) = 0;
	};
}



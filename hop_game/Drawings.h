#pragma once

#include <SFML/Graphics.hpp>
#include "Mech_components.h"
#include "Transform2D.h"

namespace hop_game
{
	class Box_drawing
	{
		sf::RectangleShape rect;

	public:

		Box_drawing()
		{
			rect.setSize({ 1.0f, 1.0f });
			rect.setOutlineThickness(0.05f);
		}

		void draw(sf::RenderTarget& t, const Transform2D& to_screen, const hop_game::Position& p, const hop_game::Box& b)
		{
			rect.setPosition(to_screen(p.val) - 0.5f * b.size);
			rect.setFillColor(b.colour);
			rect.setOutlineColor(b.outline_colour);
			rect.setSize(b.size);
			t.draw(rect);
		}
	};
}

#include <iostream>
#include <thread>

#include <SFML/Graphics.hpp>

#include "Bouncing_rabbits.h"
#include "Matrix.h"
#include "Transform2D.h"

int main()
{
	hop_game::Matrix::run_simple_matrix_tests();
	hop_game::Vector::run_simple_vector_tests();
	hop_game::Transform2D::run_silly_transform2d_tests();

	std::cout << "Hello bunnies!" << std::endl;

	hope::Entities_for_systems entities;

	sf::ContextSettings context_settings;
	context_settings.antialiasingLevel = 7;

	sf::VideoMode video_mode = sf::VideoMode(600, 400);
	sf::RenderWindow window;
	window.create(video_mode, "Less hop!", sf::Style::Close | sf::Style::Resize, context_settings);
	
	window.setFramerateLimit(60);

	if (!window.isOpen())
		std::cout << "Failed open window" << std::endl;

	
	auto view = sf::View(sf::Vector2f{ 0,0 }, sf::Vector2f{ 15, 10 });
	window.setView(view);

	auto game = hop_game::Bouncing_rabbits();


	game.Init_game(entities);


	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			game.On_event(event);

			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color(100, 100, 100, 255));

		game.Run_all_systems_for_frame(entities, window);

		window.display();
	}


	return 0;
}

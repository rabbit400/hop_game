#pragma once

#include "Vector.h"
#include <SFML/Graphics.hpp>

namespace hop_game
{
	struct Position
	{
		Float2 val;
	};
	struct Velocity
	{
		Float2 val;
	};
	struct Acceleration
	{
		Float2 val;
	};

	struct Grounded
	{
		bool grounded;
		operator bool() const { return grounded; }
	};

	struct Bouncee
	{
		float radius;
	};

	struct Bouncer
	{
		float floor_height;
		Float2 floor_normal;

		bool bounce_if_needed(
			Float2& position,
			Float2& velocity,
			float radius) const
		{
			auto p_height = position * floor_normal - floor_height - radius;
			if (p_height < 0)
			{
				position -= 2 * p_height * floor_normal;
				auto velocity_up = velocity * floor_normal;
				if (velocity_up < 0)
					velocity -= (2 * velocity_up) * floor_normal;

				return true;
			}
			return false;
		}
	};

	struct Rabbit
	{
		sf::Color Colour;
		static constexpr Float2 Size = Float2::Ones();
	};

	struct Bounding_box
	{
		float width;
		float height;
	};

	struct Box
	{
		Float2 size = { 1.0f, 1.0f };
		sf::Color colour = sf::Color::Green;
		sf::Color outline_colour = sf::Color::Magenta;
	};
}



#pragma once

#include <concepts>

template <std::invocable F>
struct Constexpr_func_result
{
	constexpr static auto value = std::invoke(F{});
};

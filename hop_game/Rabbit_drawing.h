#pragma once
#include <SFML/Graphics.hpp>
#include "Mech_components.h"
#include "Transform2D.h"

namespace hop_game
{

	struct Rabbit_drawing
	{
		sf::RectangleShape Body;
		sf::RectangleShape Head;
		sf::RectangleShape Eye;
		sf::RectangleShape Foot;
		Rabbit_drawing()
		{
			sf::Color bg_colour(255, 169, 208);
			//bg_colour = sf::Color::Magenta;
			float outline_thickness = 0.005f;

			Body.setSize({ 0.8f, 0.8f });
			Body.setPosition({ 0.0f, 0.2f });
			Body.setFillColor(bg_colour);
			Body.setOutlineColor(sf::Color::Black);
			Body.setOutlineThickness(outline_thickness);

			Head.setSize({ 0.4f, 0.4f });
			Head.setPosition({ 0.6f, 0.0f });
			Head.setFillColor(bg_colour);
			Head.setOutlineColor(sf::Color::Black);
			Head.setOutlineThickness(outline_thickness);


			Eye.setSize({ 0.15f, 0.15f });
			Eye.setPosition({ 0.7f, 0.1f });
			Eye.setFillColor(sf::Color::Red);

			Foot.setOrigin({ 0.1f, 0.1f });

			Foot.setSize({ 0.8f, 0.2f });
			Foot.setPosition({ 0.2f, 1.0f });
			Foot.setFillColor(bg_colour);
			Foot.setOutlineColor(sf::Color::Black);
			Foot.setOutlineThickness(outline_thickness);
		}

		void draw(sf::RenderTarget& t, const Transform2D& to_screen, const hop_game::Position& p,
			const hop_game::Rabbit& r, size_t frame)
		{
			Float2 translation = to_screen(p.val) - 0.5f * r.Size;
			sf::RenderStates render_states(sf::Transform().translate(translation.To_sf()));

			float l = (frame % 60) / 60.f;
			float max_rotation = 50;

			if (l <= 0.2f)
				Foot.setRotation(0.0f);
			else if (l <= 0.4)
				Foot.setRotation(max_rotation * (l - 0.2f) / 0.2f);
			else if (l <= 0.5)
				Foot.setRotation(max_rotation);
			else
				Foot.setRotation(max_rotation * (1.f - (l - 0.5f) / 0.5f));

			t.draw(Body, render_states);
			t.draw(Head, render_states);
			t.draw(Eye, render_states);
			t.draw(Foot, render_states);

			//Body.setPosition(p.val.To_sf());
			//Body.setFillColor(r.Colour);
			//Eye.setPosition(p.val.To_sf());
			//t.draw(Body);
			//t.draw(Eye);
			//t.draw(Foot);
		}
	};
}
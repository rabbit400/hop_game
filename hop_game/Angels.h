#pragma once

#include <numbers>

namespace hop_game::Angels
{
	constexpr static float radians_to_degrees_multiplier = (float)(std::numbers::inv_pi_v<double> *(360 / 2));
	constexpr static float degrees_to_radians_multiplier = (float)(2 * std::numbers::pi_v<double> / 360);

	struct Degrees;
	struct Radians;

	struct Degrees
	{
		float degrees = 0;

		constexpr operator Radians() const;

		constexpr Degrees operator  *  (const float& f)   const { return Degrees{ f * degrees }; }
		constexpr Degrees operator  +  (const Degrees& r) const { return Degrees{ r.degrees + degrees }; }
		constexpr Degrees& operator += (const Degrees& r) { degrees += r.degrees; return *this; }

		constexpr friend Degrees operator * (const float& f, const Degrees& r) { return r * f; }
	};

	struct Radians
	{
		float radians = 0;
		constexpr operator Degrees() const;

		constexpr Radians operator  *  (const float& f)   const { return Radians{ f * radians }; }
		constexpr Radians operator  +  (const Radians& r) const { return Radians{ r.radians + radians }; }
		constexpr Radians& operator += (const Radians& r)       { radians += r.radians; return *this; }

		constexpr friend Radians operator * (const float& f, const Radians& r) { return r * f; }

		static constexpr const Radians Full_circle() { return Radians{ 2 * std::numbers::pi_v<float> }; }
		static constexpr const Radians Half() { return Radians{ std::numbers::pi_v<float> }; }
		static constexpr const Radians Quarter() { return Radians{ 0.5f * std::numbers::pi_v<float> }; }
	};

	constexpr Degrees::operator Radians() const
	{
		return Radians{ degrees * degrees_to_radians_multiplier };
	}
	constexpr Radians::operator Degrees() const
	{
		return Degrees{ radians * radians_to_degrees_multiplier };
	}
};


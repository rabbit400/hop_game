#pragma once
#include "Vector.h"
#include "Mech_components.h"
#include "Entities_for_systems.h"
#include <SFML/Graphics.hpp>
#include <array>
#include "Rabbit_drawing.h"
#include "Key_states.h"
#include "Drawings.h"
#include <Component.h>
#include "IGame.h"
#include "Transform2D.h"
#include "Map.h"
#include "Ostream_operators.h"

using X = hope::X;

namespace hop_game
{
	struct Bouncing_rabbits : public IGame
	{
		enum class Movement : std::uint8_t
		{
			None, Left, Right, Up, Down
		};


		const static inline Map level_1 = Map::First_map();

		static constexpr float Ground_speed = 2.0f;
		static constexpr float Jump_speed = 5.0f;
		static constexpr float Air_acceleration = 5.0f;

		float x_min = -15.f/2;
		float x_max = 15.f/2;
		float y_min = -10.f/2;
		float y_max = 10.f/2;
		
		size_t frame = 0;
		Key_states key_states;

		static constexpr float time_per_frame = (float)(1.0 / 60);
		static constexpr Float2 default_accel = Float2::Down() * 9.8f;

		static inline const auto to_screen_transform
			= Transform2D::Scale({ {1, -1} });

		bool bounce_if_needed(
			Float2& position,
			Float2& velocity,
			const float& floor_height,
			const Float2& floor_normal)
		{
			auto p_height = position * floor_normal - floor_height;
			if (p_height < 0)
			{
				position -= 2 * p_height * floor_normal;
				auto velocity_up = velocity * floor_normal;
				if (velocity_up < 0)
					velocity -= (2 * velocity_up) * floor_normal;

				return true;
			}
			return false;
		}

		void Init_game(hope::Entities_for_systems& entities)
		{
			const auto& spawn = level_1.Spawn;
			auto lambda2 = [&](
				Position& p, Acceleration& a, Velocity& v, Grounded & g,
				Rabbit& r, Bouncee& bouncee, Bounding_box & bounding_box
				)
			{
				r.Colour = sf::Color(
					200,
					160,
					160,
					(sf::Uint8)(60)
				);
				bouncee.radius = 0.6f;
				p.val = spawn;
				v.val = { 1.7f, 0.0f };

				bounding_box = { 1.0f, 1.0f };
			};

			entities.Add_entity(std::function(lambda2));

			for (const auto& block_position : level_1.Blocks)
			{
				entities.Add_entity(std::function([&](Position& p, Box& b)
					{
						p.val = block_position;
						b = Box{}; // i.e. default size (1.0, 1.0) and colours
					}
				));
			}

			auto elems = std::array {
				std::make_tuple(x_min, Float2(1.0f, 0.0f)),
				std::make_tuple(-x_max, Float2({-1, 0})),
				std::make_tuple(y_min, Float2({0, 1})),
				std::make_tuple(-y_max, Float2({0, -1})),
			};

			for (auto& [height, normal] : elems)
				entities.Add_entity(std::function([&height, &normal](Bouncer& b)
					{
						b.floor_height = height;
						b.floor_normal = normal;
					}));
		}

		virtual void On_event(const sf::Event& event)
		{
			auto get_num_or_numpad_num = [](sf::Keyboard::Key code) -> std::optional<std::uint8_t>
			{
				switch (code)
				{
				default: return std::nullopt;
				case sf::Keyboard::Key::Num0: case sf::Keyboard::Key::Numpad0: return 0;
				case sf::Keyboard::Key::Num1: case sf::Keyboard::Key::Numpad1: return 1;
				case sf::Keyboard::Key::Num2: case sf::Keyboard::Key::Numpad2: return 2;
				case sf::Keyboard::Key::Num3: case sf::Keyboard::Key::Numpad3: return 3;
				case sf::Keyboard::Key::Num4: case sf::Keyboard::Key::Numpad4: return 4;
				case sf::Keyboard::Key::Num5: case sf::Keyboard::Key::Numpad5: return 5;
				case sf::Keyboard::Key::Num6: case sf::Keyboard::Key::Numpad6: return 6;
				case sf::Keyboard::Key::Num7: case sf::Keyboard::Key::Numpad7: return 7;
				case sf::Keyboard::Key::Num8: case sf::Keyboard::Key::Numpad8: return 8;
				case sf::Keyboard::Key::Num9: case sf::Keyboard::Key::Numpad9: return 9;
				}
			};

			switch (event.type)
			{
			default:
				break;
			case sf::Event::EventType::KeyReleased:
			{
				auto& key_event = event.key;
				key_states.released(key_event.code);
				break;
			}
			case sf::Event::EventType::KeyPressed:
			{
				auto& key_event = event.key;
				key_states.pressed(key_event.code);
				break;
			}
			}
		}

		void Run_all_systems_for_frame(hope::Entities_for_systems& entities, sf::RenderTarget& render_target)
		{
			frame += 1;

			Float2 current_movement;
			bool jump_pressed;
			bool jump_held;
			{
				current_movement = Float2::Zero();

				if (key_states[sf::Keyboard::Key::Left].is_down || key_states[sf::Keyboard::Key::A].is_down)
					current_movement += Float2::Left();
				if (key_states[sf::Keyboard::Key::Right].is_down || key_states[sf::Keyboard::Key::D].is_down)
					current_movement += Float2::Right();
				if (key_states[sf::Keyboard::Key::Up].is_down || key_states[sf::Keyboard::Key::W].is_down)
					current_movement += Float2::Up();
				if (key_states[sf::Keyboard::Key::Down].is_down || key_states[sf::Keyboard::Key::S].is_down)
					current_movement += Float2::Down();

				jump_pressed = key_states[sf::Keyboard::Key::Space].was_pressed;
				jump_held = key_states[sf::Keyboard::Key::Space].is_down;
			}

			key_states.step_frame();

			entities.Loop_all(std::function(
				[current_movement, jump_pressed, jump_held]
				(Rabbit&, Acceleration& a, Velocity& v, Grounded& g) mutable
				{
					if (g.grounded)
					{
						v.val = current_movement.X() * Ground_speed * Float2::Right();
						if (jump_pressed)
							v.val += Jump_speed * Float2::Up();

						a.val = default_accel;
					}
					else
					{
						a.val += current_movement.X() * Air_acceleration * Float2::Right();
						if (jump_held)
							a.val += Air_acceleration * Float2::Up();
					}
				}
			));

			entities.Loop_all(+[](Acceleration& a, Velocity& v)
				{
					v.val += a.val * time_per_frame;
					a.val = default_accel;
				});
			entities.Loop_all(+[](const Velocity& v, Position& p) { p.val += v.val * time_per_frame; });
			
			entities.Loop_all(
				+[](const Bouncer& bouncer, X, const Bouncee& b, Velocity& v, Position& p)
				{
					bouncer.bounce_if_needed(p.val, v.val, b.radius);
				}
			);

			entities.Loop_all(+[](Grounded& g) {g.grounded = false; });


			entities.Loop_all(std::function([]
			(const Box& obstacle, const Position& obstacle_point,
				X, Position& p, Velocity& v, Grounded& g, Bounding_box& b)
				{
					Float2 delta_pos = p.val - obstacle_point.val;
					float half_width = 0.5f * (b.width + obstacle.size.X());
					float half_height = 0.5f * (b.height + obstacle.size.Y());

					bool there_is_a_collision = abs(delta_pos.X()) < half_width
						&& abs(delta_pos.Y()) < half_height;

					if (!there_is_a_collision)
						return;

					// we know all these are positive because we have a collision
					// so |delta_x| <= half_width and |delta_y| <= half_height
					float distance_to_left   = half_width  - delta_pos * Float2::Left();
					float distance_to_right  = half_width  - delta_pos * Float2::Right();
					float distance_to_top    = half_height - delta_pos * Float2::Up();
					float distance_to_bottom = half_height - delta_pos * Float2::Down();

					float min_distance = std::min(
						std::min(distance_to_left, distance_to_right),
						std::min(distance_to_top, distance_to_bottom));

					if (distance_to_left == min_distance)
					{
						p.val.X() = (obstacle_point.val + half_width * Float2::Left()).X();
						v.val.X() = 0.0f;
					}
					else if (distance_to_right == min_distance)
					{
						p.val.X() = (obstacle_point.val + half_width * Float2::Right()).X();
						v.val.X() = 0.0f;
					}
					else if (distance_to_top == min_distance)
					{
						p.val.Y() = (obstacle_point.val + half_width * Float2::Up()).Y();
						v.val.X() = 0.0f;
						v.val.Y() = 0.0f;
						g.grounded = true;
					}
					else if (distance_to_bottom == min_distance)
					{
						p.val.Y() = (obstacle_point.val + half_width * Float2::Down()).Y();
						v.val.Y() = 0.0f;
					}
				}
			));

			sf::CircleShape shape;
			shape.setFillColor(sf::Color::Cyan);

			auto& to_screen = to_screen_transform;

			entities.Loop_all(std::function(
				[draw = Box_drawing{}, &render_target, &to_screen]
				(const Position& p, const Box& b) mutable
				{
					draw.draw(render_target, to_screen_transform, p, b);
				}
			));

			entities.Loop_all(std::function(
				[rabbit_drawing = Rabbit_drawing{}, frame = frame, &render_target, &to_screen]
				(const Position& p, const Rabbit& r) mutable
				{
					rabbit_drawing.draw(render_target, to_screen, p, r, frame);
				}
			));
		}
	};
}

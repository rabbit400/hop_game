#pragma once

#include "Matrix.h"
#include "Vector.h"
#include "Angels.h"

#include <numbers>

namespace hop_game
{
	struct Transform2D
	{
		constexpr Float2 Transform(const Float2&) const;
		constexpr Float2 Inverse(const Float2&) const;
		constexpr Float2 operator()(const Float2& f2) const { return Transform(f2); }

		constexpr Transform2D Then(const Transform2D&) const;

		constexpr static const Transform2D& Identity();

		static inline Transform2D Rotate(Angels::Radians radians); // not constexpr because sin & cos are not constexpr
		static inline Transform2D Rotate(Angels::Radians radians, Float2 center); // not constexpr because sin & cos are not constexpr
		constexpr static Transform2D Translate(Float2 addition);
		constexpr static Transform2D Scale(Float2 multipliers);
		constexpr static Transform2D Scale(Float2 multipliers, Float2 center);

		static void run_silly_transform2d_tests();

		//Transform2D() = default;
		//Transform2D(const Transform2D &) = default;
		//Transform2D(Transform2D &&) = default;
		//Transform2D& operator =(const Transform2D &) = default;
		//Transform2D& operator =(Transform2D &&) = default;
		//~Transform2D() = default;

	//private:

		Float3x3 m_matrix;
		Float3x3 m_inverse;
	};

	constexpr Float2 Transform2D::Transform(const Float2& point) const
	{
		Float3 v = point.append(1.0f);
		Float3 r = m_matrix * v;
		return r.pop_back();
	}

	constexpr Float2 Transform2D::Inverse(const Float2& point) const
	{
		Float3 v = point.append(1.0f);
		Float3 r = m_inverse * v;
		return r.pop_back();
	}

	constexpr Transform2D Transform2D::Then(const Transform2D& second) const
	{
		return Transform2D
		{
			second.m_matrix * m_matrix,
			m_inverse * second.m_inverse
		};
	}

	constexpr const Transform2D& Transform2D::Identity()
	{
		auto Lambda = []() { return Transform2D{ Float3x3::Identity(), Float3x3::Identity() }; };
		return Constexpr_func_result<decltype(Lambda)>::value;
	}

	inline Transform2D Transform2D::Rotate(Angels::Radians radians)
	{
		float sin = std::sin(radians.radians);
		float cos = std::cos(radians.radians);
		return Transform2D(
			{
				{ cos, -sin,  0 },
				{ sin,  cos,  0 },
				{   0,    0,  1 }
			},
		{
			{  cos, sin,  0 },
			{ -sin, cos,  0 },
			{   0,    0,  1 }
		});
	}
	
	inline Transform2D Transform2D::Rotate(Angels::Radians radians, Float2 center)
	{
		return Transform2D::Translate(-center).Then(Rotate(radians)).Then(Translate(center));
	}

	constexpr Transform2D Transform2D::Translate(Float2 addition)
	{
		auto [x, y] = addition;
		return Transform2D(
			{
				{ 1,  0,  x },
				{ 0,  1,  y },
				{ 0,  0,  1 }
			},
		{
			{ 1,  0, -x },
			{ 0,  1, -y },
			{ 0,  0,  1 }
		});
	}

	constexpr Transform2D Transform2D::Scale(Float2 multipliers)
	{
		auto [x, y] = multipliers;
		return Transform2D(
			{
				{ x,  0,  0 },
				{ 0,  y,  0 },
				{ 0,  0,  1 }
			},
		{
			{ 1 / x,   0,   0 },
			{  0,   1 / y,  0 },
			{  0,    0,   1 }
		});
	}

	constexpr Transform2D Transform2D::Scale(Float2 multipliers, Float2 center)
	{
		return Translate(-center).Then(Scale(multipliers)).Then(Translate(center));
	}
}


#pragma once

#include <vector>
#include "Vector.h"

namespace hop_game
{

	struct Map
	{
		Float2 Spawn;
		std::vector<Float2> Blocks;

		static Map First_map();
	};
}
#pragma once

#include <array>
#include "Vector.h"
#include "Constexpr_func_result.h"

namespace hop_game::Matrix
{
	template <size_t D>
	concept Valid_matrix_dimension = ::hop_game::Vector::Valid_vector_dimension<D>;

	template <size_t ... Ds>
	concept Valid_matrix_dimensions = (Valid_matrix_dimension<Ds> && ...);

	template <class T>
	concept Valid_matrix_element_type = ::hop_game::Vector::Valid_vector_element_type<T>;

	template <size_t M, size_t N, class T>
	concept Valid_matrix = Valid_matrix_dimensions<M, N> && Valid_matrix_element_type<T>;

	template <size_t Step, size_t N, class T>
	struct Matrix_stepper
	{
		static bool constexpr Valid_index(size_t i) { return i < N; }
		static bool constexpr Validate_index(size_t i) { if (!Valid_index(i)) throw "index out of bounds"; }

		constexpr Matrix_stepper(T* first) : m_first{ first } {}

		constexpr T& get(size_t i) const { return m_first[i * Step]; }
		constexpr T& operator [] (size_t i) const { return get(i); }
		constexpr T& at(size_t i) const { Validate_index(i); return get(i); }
		template <size_t i> requires (Valid_index(i))
			constexpr T& at() const { return get(i); }

	protected:
		T* const m_first;
	};


	template <size_t Step, size_t N, class T>
	requires (N > 0)
		struct Matrix_row;

	template <size_t Step, size_t N, class T>
	requires (N > 0)
		struct Matrix_column : Matrix_stepper<Step, N, T>
	{
		constexpr Matrix_column(T* first) : Matrix_stepper<Step, N, T>{ first } {}
		auto transpose() { return Matrix_row<Step, N, T>{this->m_first}; }
	};

	template <size_t Step, size_t N, class T>
	requires (N > 0)
		struct Matrix_row : Matrix_stepper<Step, N, T>
	{
		constexpr Matrix_row(T* first) : Matrix_stepper<Step, N, T>{ first } {}
		auto transpose() { return Matrix_column<Step, N, T>{this->m_first}; }
	};

	template <size_t Step_row, size_t Step_col, size_t N, class T_row, class T_col>
	requires (std::is_same_v<const T_row, const T_col>)
		auto operator * (const Matrix_row<Step_row, N, T_row>& row, const Matrix_column<Step_col, N, T_col>& col)
	{
		using T_result = std::remove_const_t<T_row>;
		T_result ret{};
		for (size_t i = 0; i < N; ++i)
			ret += row[i] * col[i];
		return ret;
	}

	template <size_t Step_row, size_t N, class T_row, class T_col>
	requires (std::is_same_v<const T_row, const T_col>)
		auto operator * (const Matrix_row<Step_row, N, T_row>& row, const Vector::Vector<N, T_col>& col)
	{
		using T_result = std::remove_const_t<T_row>;
		T_result ret{};
		for (size_t i = 0; i < N; ++i)
			ret += row[i] * col[i];
		return ret;
	}

	template <size_t M, size_t N, class T>
	requires (Valid_matrix<M, N, T>)
		struct Matrix
	{
		static bool constexpr Valid_row_index(size_t i) { return i < M; }
		static bool constexpr Valid_column_index(size_t i) { return i < N; }
		static bool constexpr Valid_indices(size_t m, size_t n)
		{
			return Valid_row_index(m) && Valid_column_index(n);
		}

		static void constexpr Validate_row_index(size_t i) { if (!Valid_row_index(i)) throw "OUT OF BOUNDS ROW INDEX"; }
		static void constexpr Validate_column_index(size_t i) { if (!Valid_column_index(i)) throw "OUT OF BOUNDS COLUMN INDEX"; }
		static void constexpr Validate_indices(size_t m, size_t n)
		{
			Validate_row_index(m);
			Validate_column_index(n);
		}

		constexpr auto Row(size_t m) { return Matrix_row<1, N, T>{&m_array[N * m]}; };
		constexpr auto Row(size_t m) const { return Matrix_row<1, N, const T>{&m_array[N * m]}; };
		constexpr auto Column(size_t n) { return Matrix_column<N, M, T>{&m_array[n]}; };
		constexpr auto Column(size_t n) const { return Matrix_column<N, M, const T>{&m_array[n]}; };

		constexpr auto Row_at(size_t m) { Validate_row_index(m); return Row(m); };
		constexpr auto Row_at(size_t m) const { Validate_row_index(m); return Row(m); };
		constexpr auto Column_at(size_t n) { Validate_column_index(n); return Column(n); }
		constexpr auto Column_at(size_t n) const { Validate_column_index(n); return Column(n); }

		template <size_t m> requires (Valid_row_index(m)) constexpr auto Row() { return Row(m); };
		template <size_t m> requires (Valid_row_index(m)) constexpr auto Row() const { return Row(m); };
		template <size_t n> requires (Valid_column_index(n)) constexpr auto Column() { return Column(n); };
		template <size_t n> requires (Valid_column_index(n)) constexpr auto Column() const { return Column(n); };

		constexpr auto& operator()(size_t m, size_t n) { return Row(m)[n]; }
		constexpr auto& operator()(size_t m, size_t n) const { return Row(m)[n]; }

		constexpr auto& at(size_t m, size_t n) { Validate_indices(m, n); return *this(m, n); }
		constexpr auto& at(size_t m, size_t n) const { Validate_indices(m, n); return *this(m, n); }

		template<size_t m, size_t n> requires (Valid_indices(m, n)) constexpr auto& at() { return *this(m, n); }
		template<size_t m, size_t n> requires (Valid_indices(m, n)) constexpr auto& at() const { return *this(m, n); }


		static constexpr void for_each_index(std::invocable<size_t, size_t> auto&& f)
		{
			for (size_t m = 0; m < M; ++m)
				for (size_t n = 0; n < N; ++n)
					f(m, n);
		}

		constexpr void for_each(std::invocable<T&> auto&& f)
		{
			for (size_t m = 0; m < M; ++m)
				for (size_t n = 0; n < N; ++n)
					f((*this)(m, n));
		}

		Matrix() = default;
		Matrix(const Matrix&) = default;
		Matrix(Matrix&&) = default;
		Matrix& operator = (const Matrix&) = default;
		Matrix& operator = (Matrix&&) = default;
		~Matrix() = default;

		constexpr Matrix(std::invocable<size_t, size_t> auto&& f)
		{
			for_each_index([&](size_t m, size_t n)
				{
					(*this)(m, n) = static_cast<const T&>(f(m, n));
				});
		}

		template <std::size_t ... Dims>
		requires (M == sizeof...(Dims) && ((N == Dims) && ... ))
			constexpr Matrix(T const (&...rows)[Dims])
		{
			size_t m = 0; (Set_row(m++, rows), ...);
		}

		constexpr Matrix(const std::same_as<Vector::Vector<N, T>> auto & ... rows) requires (M == sizeof...(rows))
		{
			size_t m = 0; (Set_row(m++,rows), ...);
		}

		static constexpr const Matrix& Identity() requires (N == M)
		{
			constexpr auto Lambda = [](){
				return Matrix([](size_t m, size_t n) constexpr { return (m == n ? 1 : 0); });
			};
			return Constexpr_func_result<decltype(Lambda)>::value;
		};

	private:
		std::array<T, M* N> m_array;

		void Set_row(size_t m, const auto& row)
		{
			for (size_t n = 0; n < N; ++n)
				(*this)(m, n) = row[n];
		}
	};

	// deduction guide for braced init constructor
	template <size_t N, class T, std::same_as<T> ... Ts>
	Matrix(T const (&row)[N], Ts const  (&...rows)[N])
		->Matrix<1 + sizeof...(Ts), N, T>;

	// deduction guide for braced init constructor for list of Vector
	template <size_t N, class T, std::same_as<T> ... Ts>
	Matrix(const Vector::Vector<N, T>& v, const Vector::Vector<N, Ts>& ... vs)
		->Matrix<1 + sizeof...(Ts), N, T>;

	template <size_t M, size_t N, class T>
	constexpr auto operator *(const Matrix<M, N, T>& x, const ::hop_game::Vector::Vector<N, T>& v)
	{
		return ::hop_game::Vector::Vector<M, T>([&](size_t i) { return x.Row(i) * v; });
	}

	template <size_t M, size_t N, class T>
	constexpr auto& operator *=(Matrix<M, N, T>& x, const T& scalar)
	{
		x.for_each([&](T& v) { v *= scalar; });
		return x;
	}

	template <size_t M, size_t N, class T>
	constexpr auto operator * (const Matrix<M, N, T>& x, const T& scalar)
	{
		return Matrix<M, N, T>([&](size_t m, size_t n) { return x(m, n) * scalar; });
	}

	template <size_t M, size_t N, class T>
	constexpr auto operator * (const T& scalar, const Matrix<M, N, T>& x)
	{
		return Matrix<M, N, T>([&](size_t m, size_t n) { return scalar * x(m, n); });
	}

	template <size_t M, size_t N, class T>
	constexpr auto& operator +=(Matrix<M, N, T>& x, const Matrix<M, N, T>& y)
	{
		x.for_each_index([&](size_t m, size_t n) { x(m, n) += y(m, n); });
		return x;
	}

	template <size_t M, size_t N, class T>
	constexpr auto operator +(const Matrix<M, N, T>& x, const Matrix<M, N, T>& y)
	{
		return Matrix<M, N, T>{ [&](size_t m, size_t n) { return x(m, n) + y(m, n); } };
	}

	template <size_t M1, size_t N1, class T, size_t M2, size_t N2>
	requires (N1 == M2)
		constexpr auto operator * (const Matrix<M1, N1, T>& m1, const Matrix<M2, N2, T>& m2)
	{
		return Matrix<M1, N2, T>([&](size_t m, size_t n) { return m1.Row(m) * m2.Column(n); });
	}

	void run_simple_matrix_tests();
}

namespace hop_game
{
	using Float2x2 = Matrix::Matrix<2, 2, float>;
	using Float3x3 = Matrix::Matrix<3, 3, float>;
	using Float4x4 = Matrix::Matrix<4, 4, float>;
}

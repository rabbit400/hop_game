#pragma once

#include <ostream>
#include "Vector.h"

template <size_t N, class T>
std::ostream& operator << (std::ostream& o, const hop_game::Vector::Vector<N, T> v)
{
	o << v[0];
	for (size_t i = 1; i < N; ++i)
		o << ' ' << v[i];
	return o;
}



#pragma once

#include <array>
#include <cmath>
#include <SFML/System/Vector2.hpp>
#include "Constexpr_func_result.h"

namespace hop_game::Vector
{

	inline constexpr size_t Max_vector_dimension = 10000;

	template <size_t D>
	concept Valid_vector_dimension = 0 < D && D <= Max_vector_dimension;

	template <class T>
	concept Valid_vector_element_type = (!std::is_const_v<T>) && (!std::is_reference_v<T>);

	template <size_t N, class T>
	concept Valid_vector = Valid_vector_dimension<N> && Valid_vector_element_type<T>;


	template <size_t N, class T>
	requires (Valid_vector<N, T>)
	struct Vector
	{
		static constexpr bool Valid_index(size_t i) { return i < N; }
		static constexpr void Validate_index(size_t i) { if (!Valid_index(i)) throw "INDEX OUT OF BOUNDS"; }

		constexpr       T& get(size_t i)       { return m_array[i]; }
		constexpr const T& get(size_t i) const { return m_array[i]; }

		constexpr       T& at(size_t i)       { Validate_index(i); return get(i); }
		constexpr const T& at(size_t i) const { Validate_index(i); return get(i); }
		
		// TODO: probabyl remove one of the templates for at/get
		// initially i thought "at" made more sense as name
		// now adding get cause that matches what std lib expects for structured bindings
		template <size_t i> requires (Valid_index(i)) constexpr       T& at()        { return get(i); }
		template <size_t i> requires (Valid_index(i)) constexpr const T& at()  const { return get(i); }
		template <size_t i> requires (Valid_index(i)) constexpr       T& get()       { return get(i); }
		template <size_t i> requires (Valid_index(i)) constexpr const T& get() const { return get(i); }

		constexpr       T& operator [](size_t i)       { return get(i); }
		constexpr const T& operator [](size_t i) const { return get(i); }

		constexpr       T& X()                        { return at<0>(); }
		constexpr const T& X() const                  { return at<0>(); }
		constexpr       T& Y()       requires (N > 1) { return at<1>(); }
		constexpr const T& Y() const requires (N > 1) { return at<1>(); }
		constexpr       T& Z()       requires (N > 2) { return at<1>(); }
		constexpr const T& Z() const requires (N > 2) { return at<1>(); }

		constexpr auto append(const std::same_as<T> auto& ...appendicies) const
		{
			return [&]<size_t ...Is>(std::index_sequence<Is...>)
			{
				return Vector<N + sizeof...(appendicies), T>(this->at<Is>() ..., appendicies...);
			}(std::make_index_sequence<N>());
		}

		template <size_t n = 1>
		constexpr auto pop_back() const requires (n < N)
		{
			return[&]<size_t ...Is>(std::index_sequence<Is...>)
			{
				return Vector<N - n, T>(this->at<Is>() ...);
			}(std::make_index_sequence<N-n>());
		}

		static constexpr void for_each_index(std::invocable<size_t> auto&& f)
		{
			for (size_t i = 0; i < N; ++i)
				f(i);
		}

		constexpr void for_each(std::invocable<T&> auto&& f)
		{
			for (size_t i = 0; i < N; ++i)
				f(get(i));
		}
		
		constexpr T abs_squared() const requires std::floating_point<T>
		{
			return this->dot(*this); // dot product with ourselves
		}

		T abs() const requires std::floating_point<T>
		{
			if constexpr (1 == N)
				return std::fabs(get(0));
			else if constexpr (2 == N)
				return std::hypot(get(0), get(1));
			else if constexpr (3 == N)
				return std::hypot(get(0), get(1), get(2));
			else 
				return std::sqrt(abs_squared());
		}

		void normalize() requires std::floating_point<T>
		{
			for_each([m = 1 / abs()](T& val) { val *= m; });
		}

		Vector normalized() const requires std::floating_point<T>
		{
			return Vector([this, m = 1 / abs()](size_t i) { return m * (*this)[i]; });
		}

		constexpr T dot(const Vector& v) const
		{
			T ret{};
			for_each_index([&](size_t i) { ret += get(i) * v[i]; });
			return ret;
		}

		constexpr T cross(const Vector& v) const requires (N == 2)
		{
			return get(0) * v[1] - get(1) * v[0];
		}

		constexpr T cross(const Vector& v) const requires (N == 3)
		{
			auto cross_i = [&](size_t i)
			{
				size_t j = (i + 1) % 3;
				size_t k = (i + 2) % 3;
				return get(j) * v[k] - get(k) * v[j];
			};
			return Vector<N, T>{ cross_i(0), cross_i(1), cross_i(2) };
		}

		Vector() = default;
		Vector(const Vector&) = default;
		Vector(Vector&&) = default;
		Vector& operator = (const Vector&) = default;
		Vector& operator = (Vector&&) = default;
		~Vector() = default;

		constexpr Vector(std::invocable<size_t> auto && f)
		{
			for_each_index([&](size_t i) { get(i) = static_cast<const T&>(f(i)); });
		}

		constexpr Vector(const std::same_as<T> auto& ... args) requires (N == sizeof...(args))
			: m_array{ args... }
		{}

		// this allows Vector<2, float>{{1, 2}} where 1 and 2 are not strict floats (note double braces)
		constexpr Vector( T const (&arr)[N])
			: Vector{ [&](size_t i) { return arr[i]; } }
		{}

		static constexpr const Vector& Zero()
		{
			constexpr auto Lambda = []() { return Vector([](size_t) { return 0; }); };
			return Constexpr_func_result<decltype(Lambda)>::value;
		}

		static constexpr const Vector& Ones()
		{
			constexpr auto Lambda = []() { return Vector([](size_t) { return 1; }); };
			return Constexpr_func_result<decltype(Lambda)>::value;
		}

		static constexpr const Vector& Halfs()
		{
			constexpr auto Lambda = []() { return Vector([](size_t) { return 0.5f; }); };
			return Constexpr_func_result<decltype(Lambda)>::value;
		}

		template <size_t i>
		static constexpr const Vector& Positive_unit_vector() requires (i < N)
		{
			constexpr auto Lambda = []() { return Vector([](size_t ii) { return ii == i ? 1 : 0; }); };
			return Constexpr_func_result<decltype(Lambda)>::value;
		}
		template <size_t i>
		static constexpr const Vector& Negative_unit_vector() requires (i < N)
		{
			constexpr auto Lambda = []() { return Vector([](size_t ii) { return ii == i ? -1 : 0; }); };
			return Constexpr_func_result<decltype(Lambda)>::value;
		}

		static constexpr const Vector& Left()                   { return Negative_unit_vector<0>(); }
		static constexpr const Vector& Right()                  { return Positive_unit_vector<0>(); }
		static constexpr const Vector& Up()    requires (N > 1) { return Positive_unit_vector<1>(); }
		static constexpr const Vector& Down()  requires (N > 1) { return Negative_unit_vector<1>(); }

		sf::Vector2f To_sf() const requires (N == 2) { return sf::Vector2f(X(), Y()); }
		sf::Vector2f To_sf(const T & multiplier) const requires (N == 2) { return sf::Vector2f(X() * multiplier, Y() * multiplier); }
		operator sf::Vector2f() const requires (N == 2) { return To_sf(); }

	private:
		std::array<T, N> m_array;
	};

	// deduction guide for braced init constructor
	template <class T, std::same_as<T> ... Ts>
	Vector(const T& arg, const Ts & ... args)->Vector<1 + sizeof...(Ts), T>;

	template <size_t N, class T>
	constexpr auto operator-(const Vector<N, T>& v)
	{
		return Vector<N, T>([&](size_t i) {return -v[i]; });
	}

	template <size_t N, class T>
	constexpr bool operator==(const Vector<N, T>& v, const Vector<N, T>& w)
	{
		for (size_t i = 0; i < N; ++i) if (v[i] != w[i]) return false;
		return true;
	}

	template <size_t N, class T>
	constexpr auto& operator*=(Vector<N, T>& v, const T& scalar)
	{
		v.for_each([&](T& elem) {elem *= scalar; });
		return v;
	}
	template <size_t N, class T>
	constexpr auto operator*(const Vector<N, T>& v, const T& scalar)
	{
		return Vector<N, T>([&](size_t i) { return v[i] * scalar; });
	}
	template <size_t N, class T>
	constexpr auto operator*(const T& scalar, const Vector<N, T>& v)
	{
		return Vector<N, T>([&](size_t i) { return scalar * v[i]; });
	}

	template <size_t N, class T>
	constexpr auto operator+(const Vector<N, T>& v, const Vector<N, T>& w)
	{
		return Vector<N, T>([&](size_t i) { return v[i] + w[i]; });
	}
	template <size_t N, class T>
	constexpr auto operator-(const Vector<N, T>& v, const Vector<N, T>& w)
	{
		return Vector<N, T>([&](size_t i) { return v[i] - w[i]; });
	}
	template <size_t N, class T>
	constexpr T operator*(const Vector<N, T>& v, const Vector<N, T>& w)
	{
		return v.dot(w);
	}

	template <size_t N, class T>
	constexpr auto& operator+=(Vector<N, T>& v, const Vector<N, T>& w)
	{
		v.for_each_index([&](size_t i) { v[i] += w[i]; });
		return v;
	}
	template <size_t N, class T>
	constexpr auto& operator-=(Vector<N, T>& v, const Vector<N, T>& w)
	{
		v.for_each_index([&](size_t i) { v[i] -= w[i]; });
		return v;
	}

	void run_simple_vector_tests();
}

namespace std
{
	template<size_t N, class T>
	struct tuple_size<::hop_game::Vector::Vector<N, T>>
		: std::integral_constant<size_t, N> {};

	template<size_t I, size_t N, class T>
	struct tuple_element<I, ::hop_game::Vector::Vector<N, T>>
		: std::type_identity<T> {};
}

namespace hop_game
{
	using Float2 = Vector::Vector<2, float>;
	using Float3 = Vector::Vector<3, float>;
}

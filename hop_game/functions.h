#pragma once
namespace hop_game::functions
{
	constexpr float clamp(float f, float min = 0.0f, float max = 1.0f);

	template <class T>
	constexpr T lerp(float t, const T& t0, const T& t1)
	{
		float tc = clamp(t);
		return t0 * (1.0f - tc) + t1 * tc;
	}
};




#include <iostream>
#include "Entities_for_systems.h"


using namespace hope;
using namespace std;

template <class T>
struct DC
{
	T value;
	operator T& () { return value; }
	operator const T& () const { return value; }
};

static_assert(hopu::In_list_v<int, double, float, int>);
static_assert(!hopu::Distinct_v<int, double, float, int>);
static_assert(!hopu::In_list_v<int, double, float>);
static_assert(hopu::Distinct_v<int, double, float>);

void sdf(DC<int>& a, DC<float>& b)
{
	a.value = -56;
	b.value = -59;
};

int main()
{
	cout << "Hello hoppers!" << endl;
	cout << "This main only writes silly test output." << endl;

	Entities_for_systems entities;


	entities.Add_entity(sdf);

	entities.Add_entity<DC<int>, DC<double>, DC<float>>(
		[](int& uwu, DC<double>& silly, const float&) {
			uwu = -3;
			silly.value += 98;
			std::cout << " APPLIED VALUES !!!!! int: " << uwu << " DCintPTR: " << &uwu << std::endl;
		}
	);
	entities.Add_entity(
		+[](DC<int>& uwu) {uwu.value = -321; }
	);
	entities.Add_entity(
		std::function([](DC<int>& uwu) {uwu.value = -345; })
	);

	size_t n;

	n = 0;
	auto li = [&n](int& i) { ++n; std::cout << "int" << std::endl; };
	entities.Loop_all<DC<int>&>(li);
	std::cout << n << " num entities" << std::endl;

	n = 0;
	entities.Loop_all(std::function(
		[&n](DC<int>& i, const DC<float>& f)
		{
			++n;
			std::cout << "int+float: " << i << ' ' << f << std::endl;
		}));
	std::cout << n << " num entities" << std::endl;

	n = 0;
	entities.Loop_all<DC<int>&, DC<double>&>(
		[&n](int& i, double& d)
		{
			++n;
			std::cout << "int+double: " << i << ' ' << d << std::endl;
		});
	std::cout << n << " num entities" << std::endl;

	n = 0;
	entities.Loop_all<DC<int>&, DC<double>&, DC<float>&>(
		[&n](int& i, double& d, const float& f)
		{
			++n;
			std::cout << "int+double+float: " << i << ' ' << d << ' ' << f << std::endl;
			std::cout << "intPTR: " << &i << std::endl;
			i += 13;
		});
	std::cout << n << " num entities" << std::endl;

	n = 0;
	entities.Loop_all<DC<int>&, DC<double>&, DC<float>&>(
		[&n](int& i, double& d, const float& f)
		{
			++n;
			std::cout << "int+double+float: " << i << ' ' << d << ' ' << f << std::endl;
			std::cout << "intPTR: " << &i << std::endl;
			i += 13;
		});
	std::cout << n << " num entities" << std::endl;

	return 0;
}
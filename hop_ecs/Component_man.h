#pragma once

#include <memory>
#include <cstdint>
#include <vector>

#include "Component.h"
#include "Component_status.h"


namespace hope
{
	class IComponent_man
	{
	public:
		virtual ~IComponent_man() {}
		virtual void Add_entities(size_t n) = 0;
		virtual void Clear_modifications() = 0;
	};

	template<Component C>
	class Component_man : public IComponent_man
	{
	private:
		std::vector<Component_status> Statuses;
		std::vector<std::unique_ptr<C>> Component_ptrs;

	public:

		virtual ~Component_man() {}

		virtual void Clear_modifications()
		{
			for (size_t i = 0; i <= Statuses.size(); ++i)
			{
				auto& status_store = Statuses[i];

				if (status_store.Removed() && !status_store.Exists())
				{
					Component_ptrs[i] = nullptr;
				}

				status_store.Unset_added();
				status_store.Unset_removed();
			}
		}

		virtual void Add_entities(size_t n)
		{
			size_t new_size = Statuses.size() + n;
			Statuses.resize(new_size);
			Component_ptrs.resize(new_size);
		}

		bool Try_create(size_t id)
		{
			Component_status& status = Statuses.at(id);

			if (status.Exists())
				return false;

			auto& component_ptr = Component_ptrs.at(id);

			if (component_ptr)
				Reset_component(*component_ptr);
			else
				component_ptr = std::make_unique<C>();

			status.Set_exist();
			status.Set_added();
			return true;
		}

		bool Exists(size_t id) const
		{
			return Statuses.at(id).Exists();
		}

		C* Try_get(size_t id)
		{
			if (Exists(id))
				return Component_ptrs[id].get();
			else
				return nullptr;
		}

		C const* Try_get(size_t id) const
		{
			if (Exists(id))
				return Component_ptrs[id].get();
			else
				return nullptr;
		}

		bool Remove(size_t id)
		{
			Component_status& status_store = Statuses.at(id);

			if (status_store.Exists())
			{
				status_store.Unset_exist();
				status_store.Set_removed();
				return true;
			}
			else
			{
				return false;
			}
		}
	};
}


#pragma once

#include <cstdint>

namespace hope
{
	class Component_status
	{
		const uint8_t No_flags = 0;
		const uint8_t Exist_flag = 1;
		const uint8_t Removed_flag = 2;
		const uint8_t Added_flag = 4;

		uint8_t status = No_flags;

		bool Read_flag(uint8_t flag) const { return (status & flag) != 0; }
		void Set_flag(uint8_t flag) { status |= flag; }
		void Unset_flag(uint8_t flag) { status &= ~flag; }

	public:
		bool Exists() const { return Read_flag(Exist_flag); }
		void Set_exist() { Set_flag(Exist_flag); }
		void Unset_exist() { Unset_flag(Exist_flag); }

		bool Removed() const { return Read_flag(Removed_flag); }
		void Set_removed() { Set_flag(Removed_flag); }
		void Unset_removed() { Unset_flag(Removed_flag); }

		bool Added() const { return Read_flag(Added_flag); }
		void Set_added() { Set_flag(Added_flag); }
		void Unset_added() { Unset_flag(Added_flag); }
	};
}

#pragma once

#include <type_traits>
#include "hop_utils.h"

namespace hope
{
	template <class C>
	struct Is_valid_component
	{
		static constexpr bool value = std::is_class_v<C> &&
			std::is_default_constructible_v<C> &&
			std::is_move_assignable_v<C> &&
			std::is_move_constructible_v<C> &&
			std::is_destructible_v<C>;
	};

	template <class C>
	concept Component
		= Is_valid_component<C>::value;

	template <class C>
	concept Component_const
		= (std::is_const_v<C> &&
			Component<std::remove_const_t<C>>
			);

	template <class C>
	concept Component_maybe_const
		= (Component<C> ||
			Component_const<C>
			);

	template <class C>
	concept Component_ref
		= (std::is_reference_v<C> &&
			Component<std::remove_reference_t<C>>
			);

	template <class C>
	concept Component_const_ref
		= (std::is_reference_v<C> &&
			Component_const<std::remove_reference_t<C>>
			);

	template <class C>
	concept Component_maybe_const_ref
		= (std::is_reference_v<C> &&
			Component_maybe_const<std::remove_reference_t<C>>
			);

	template <Component C> void Reset_component(C& c)
	{
		c = C();
	}


	template <Component C>
	struct Not
	{
		using Underlying_component = C;
	};

	template <class C>
	struct Is_valid_component<Not<C>>
		: std::false_type {};

	template <class T>
	struct Is_not : std::false_type {};

	template <Component C>
	struct Is_not<Not<C>> : std::true_type {};

	template <class T>
	concept Not_decayed = Is_not<std::decay_t<T>>::value;

	template <Component_maybe_const C>
	struct Maybe
	{
		using Underlying_component = std::decay_t<C>;
		static constexpr bool Is_const = std::is_const_v<C>;
		Maybe(C* p_component) : m_component { p_component } {}

		bool has_value() const { return m_component != nullptr; }
		C& value() const { return *m_component; }

		operator bool() const { return has_value(); }
		C& operator *() { return value(); }
		C* operator->() { return m_component; }

	private:
		C * m_component;
	};

	template <class C>
	struct Is_valid_component<Maybe<C>>
		: std::false_type {};

	template <class C>
	struct Is_maybe : std::false_type {};

	template <Component_maybe_const C>
	struct Is_maybe<Maybe<C>> : std::true_type {};

	template <class C>
	concept Maybe_of_mutable_component = Is_maybe<C>::value && !C::Is_const;
	template <class C>
	concept Maybe_of_const_component = Is_maybe<C>::value && C::Is_const;

	template <class T>
	concept Valid_argument
		= Component_maybe_const_ref<T>
		|| Not_decayed<T>
		|| Maybe_of_const_component<T>
		|| Maybe_of_mutable_component<T>;

	template <class ... Args>
	struct Valid_argument_list_s : std::false_type {};

	template <Component_maybe_const_ref First, Valid_argument ... Args>
	struct Valid_argument_list_s<First, Args...> : std::true_type {};

	template <Valid_argument First, Valid_argument ... Args>
		requires (!Component_maybe_const_ref<First>)
	struct Valid_argument_list_s<First, Args...> : Valid_argument_list_s<Args...> {};

	template <class ... Args>
	inline constexpr bool Valid_argument_list_v = Valid_argument_list_s<Args...>::value;

	template <class ... Args>
	concept Valid_argument_list = Valid_argument_list_v<Args...>;

	struct static_assert_tests {
		struct Dum {};

		static_assert(!Valid_argument<Dum>);
		static_assert(Valid_argument<Dum&>);
		static_assert(!Valid_argument<const Dum>);
		static_assert(Valid_argument<const Dum&>);

		static_assert(Valid_argument<Not<Dum>>);
		static_assert(Valid_argument<Not<Dum>&>);
		static_assert(Valid_argument<const Not<Dum>>);
		static_assert(Valid_argument<const Not<Dum>&>);

		static_assert(!Valid_argument_list<Dum>);
		static_assert(Valid_argument_list<Dum&>);
		static_assert(!Valid_argument_list<const Dum>);
		static_assert(Valid_argument_list<const Dum&>);

		static_assert(!Valid_argument_list<Not<Dum>>);
		static_assert(!Valid_argument_list<Not<Dum>&>);
		static_assert(!Valid_argument_list<const Not<Dum>>);
		static_assert(!Valid_argument_list<const Not<Dum>&>);

		static_assert(Valid_argument_list<Not<Dum>, Dum&>);
		static_assert(!Valid_argument_list<Not<Dum>, Dum>);

		static_assert(Valid_argument_list<Dum&, Not<Dum>>);
		static_assert(!Valid_argument_list<Dum, Not<Dum>>);

		static_assert(!Valid_argument_list<Not<Dum>>);
	};


}

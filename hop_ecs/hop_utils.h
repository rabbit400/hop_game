#pragma once

#pragma once

#include <tuple>
#include <type_traits>
#include <optional>
#include <utility>
#include <functional>

namespace hopu
{
	template <class H, class ... Ts>
	using Head_t = H;

	template <class X, class ... List>
	constexpr bool In_list_v = sizeof...(List) > 0
		&& (std::is_same_v<X, List> || ...);

	template <class ... Ts>
	struct Distinct_s;

	template <>
	struct Distinct_s<> : std::bool_constant<true> {};

	template <class H, class ... Tail>
	struct Distinct_s<H, Tail...>
		: std::bool_constant<Distinct_s<Tail...>::value && !In_list_v<H, Tail...>> {};

	template <class ... Ts>
	constexpr bool Distinct_v = Distinct_s<Ts...>::value;

	template<class Callable, class ... Args>
	concept invocable_on_each = (std::invocable<Callable, Args> && ...);

	template<class Callable, class ... Args>
	concept nonvoid_invocable =
		(std::invocable<Callable, Args...>
			&& !std::is_void_v<std::invoke_result_t<Callable, Args...>>);

	template<class Callable, class ... Args>
	concept nonvoid_invocable_on_each = (nonvoid_invocable<Callable, Args> && ...);

	template <class Callable, class ... Ts>
	requires (nonvoid_invocable<Callable, Ts> && ...)
		auto invoke_on_each(Callable&& f, Ts && ... args)
	{
		return std::tuple<std::invoke_result_t<Callable, Ts> ...>(
			std::invoke(std::forward<Callable>(f), std::forward<Ts>(args))...
			);
	}

	template <class Callable, class Tuple>
	auto apply_on_each(Callable&& f, Tuple&& tuple)
	{
		auto lambda = [&f]<class...  Ts>(Ts&& ... elems)
		{
			return invoke_on_each(std::forward<Callable>(f), std::forward<Ts>(elems)...);
		};
		return std::apply(lambda, std::forward<Tuple>(tuple));
	}
}


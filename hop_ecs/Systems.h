#pragma once

#include "Component.h"

namespace hope
{
	struct Carthesian {};

	using X = Carthesian;

	template <>
	struct Is_valid_component<Carthesian>
		: std::false_type {};

	template <class C>
	concept Carthesian_decayed
		= std::is_same_v<Carthesian, std::decay_t<C>>;

	template <class C>
	concept Not_carthesian_decayed
		= !Carthesian_decayed<C>;


	template <class ... Ts>
	struct First_carthesian_s
	{
		static constexpr std::optional<size_t> value{}; // default empty
	};
	template <Carthesian_decayed C, class ... Ts>
	struct First_carthesian_s<C, Ts...>
	{
		static constexpr std::optional<size_t>  value = 0;
	};
	template <Not_carthesian_decayed T, class ... Ts>
	requires (First_carthesian_s<Ts...>::value.has_value())
		struct First_carthesian_s<T, Ts...>
	{
		static constexpr std::optional<size_t> value = 1 + First_carthesian_s<Ts...>::value.value();
	};

	template <class ... Ts>
	inline constexpr std::optional<size_t> First_carthesian_v = First_carthesian_s<Ts...>::value;

	struct static_assert_tests_First_carthesian_v {
		struct Dum {};
		static_assert(First_carthesian_v<X>.value() == 0);
		static_assert(First_carthesian_v<Dum&, X, Dum&, Dum&>.value() == 1);
		static_assert(First_carthesian_v<Dum, int, X, Dum&, X&, Dum&>.value() == 2);
	};


	template <class ... Args>
	struct Valid_system_argument_list_s : std::false_type {};

	template <class ... Args>
	requires (Valid_argument_list<Args...>)
		struct Valid_system_argument_list_s<Args...> : std::true_type {};

	template <class ... Args>
	requires (First_carthesian_v<Args...>.has_value())
		struct Valid_system_argument_list_s<Args...>
	{
		static constexpr size_t X_index = First_carthesian_v<Args...>.value();

		// these work in gcc trunk, clang trunk, msvc 19.30 (but not msvc 19.29)
		static constexpr bool head_valid = Packs::Pack<Args...>
			::template Take<X_index>
			::template Apply<Valid_argument_list_s>::value;
		//static constexpr bool tail_valid = Packs::Pack<Args...>
		//    ::template Skip<1 + X_index>
		//    ::template Apply<Valid_system_argument_list_s>::value;

		// using this for tail works in msvc 19.29
		// BUT its is a different result - is not true for multiple X divided groups
		// presumably the difference is that there is no template recursion on Valid_list_s
		static constexpr bool tail_valid = Packs::Pack<Args...>
			::template Skip<1 + X_index>
			::template Apply<Valid_argument_list_s>::value;
	
		static constexpr bool value = head_valid && tail_valid;
	};

	template <class ... Args>
	inline constexpr bool Valid_system_argument_list_v = Valid_system_argument_list_s<Args...>::value;

	template <class ... Args>
	concept Valid_system_argument_list = Valid_system_argument_list_v<Args...>;

	struct static_assert_tests_ {
		struct Dum {};

		static_assert(First_carthesian_v<const Dum&, X, Dum&>.has_value());
		static_assert(!Valid_argument_list<const Dum&, X, Dum&>);
		static_assert(Valid_argument_list<Dum&>);
		static_assert(Valid_argument_list<const Dum&>);

		// some typical valid lists
		static_assert(Valid_system_argument_list<Dum&>);
		static_assert(Valid_system_argument_list<const Dum&, X, Dum&>);
		static_assert(Valid_system_argument_list<Dum&, const Dum&, X, Dum&, Dum&>);

		// this list is not valid because it contains 2 X
		// we want it to be valid but currently waiting for microsoft to update compiler for visual studio 2019
		// ... the compiler in visual studio 2022 works
		static_assert(!Valid_system_argument_list<Dum&, X, const Dum&, X, Dum&, Dum&>);

		// some invalid special cases
		static_assert(!Valid_system_argument_list<>);
		static_assert(!Valid_system_argument_list<Dum>);

		// some invalid delimitations
		static_assert(!Valid_system_argument_list<X>);
		static_assert(!Valid_system_argument_list<const Dum&, X>);
		static_assert(!Valid_system_argument_list<X, Dum&>);
	};
}

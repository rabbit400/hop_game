#pragma once

#include "Entity_man.h"
#include "Systems.h"

namespace hope
{
	class Entities_for_systems
	{
		Entity_man entities;

		template<class... Args>
		void Loop_all_impl(std::invocable<Args...> auto&& f);

		template <class ... Args>
		requires (Valid_system_argument_list_v<Args...>)
			struct Dividend
		{
			template <class ... Tail>
			struct Tail_s
			{
				template <class ... Head>
				requires (Valid_argument_list_v<Head...>)
					struct Head_s
				{
					static void loop_all(std::invocable<Args...> auto&& f, Entities_for_systems& entities)
					{
						if constexpr (sizeof...(Tail) == 0)
						{
							entities.entities.Loop_all<Head...>(f);
						}
						else
						{
							auto f_head = [&entities, &f](Head... head_args)
							{
								auto f_tail = [&](Tail... tail_args)
								{
									f(head_args..., X{}, tail_args...);
								};
								entities.Loop_all_impl<Tail...>(f_tail);
							};

							entities.entities.Loop_all<Head...>(f_head);
						}
					}
				};
			};

			using P = Packs::Pack<Args...>;
			static constexpr size_t Head_size = []()
			{
				if constexpr (P::template Transform<std::decay>::template Contains<X>)
					return  P::template Transform<std::decay>::template Index_of<X>;
				else
					return sizeof...(Args);
			}();

			// +1 for the X delimiter. (Skip on an empty pack returns empty pack.)
			using Tail_pack = P::template Skip<Head_size + 1>;
			using Head_pack = P::template Take<Head_size>;

			using Tail_struct = Tail_pack::template Apply<Tail_s>;
			using Head_struct = Head_pack::template Apply<Tail_struct::Head_s>;

			static void loop_all(std::invocable<Args...> auto&& f, Entities_for_systems& entites)
			{
				Head_struct::loop_all(f, entites);
			}
		};

		template<class... Args>
		requires (Valid_system_argument_list_v<Args...>)
			void Loop_all_impl(std::invocable<Args...> auto&& f)
		{
			Dividend<Args...>::loop_all(f, *this);
		}

	public:

		template<class... Args>
		requires (Valid_system_argument_list_v<Args...>)
			void Loop_all(std::invocable<Args...> auto&& f)
		{
			Loop_all_impl<Args...>(f);
		}
		template<class... Args>
		requires (Valid_system_argument_list_v<Args...>)
			void Loop_all(void (*fptr)(Args...))
		{
			Loop_all_impl<Args...>(fptr);
		}
		template<class... Args>
		requires (Valid_system_argument_list_v<Args...>)
			void Loop_all(std::function<void(Args  ...)> f)
		{
			Loop_all_impl<Args...>(f);
		}

		template <Component ... Components>
		void Add_entity(std::invocable<Components&...> auto&& f)
		{
			return entities.Add_entity<Components...>(f);
		}
		template <Component ... Components>
		void Add_entity(void (*fptr)(Components & ...))
		{
			return entities.Add_entity<Components...>(fptr);
		}
		template <Component ... Components>
		void Add_entity(std::function<void(Components & ...)> f)
		{
			return entities.Add_entity<Components...>(f);
		}
	};


}

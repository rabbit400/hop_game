#pragma once

#include <typeindex>
#include <unordered_map>

#include "Component.h"
#include "Component_man.h"

#include "Packs.h"

namespace hope
{
	class Entity_man
	{
		using Component_type_id = std::type_index;

		size_t Num_entities = 0;
		std::unordered_map<Component_type_id, std::unique_ptr<IComponent_man>> m_cmans;

		size_t Add_entity_private()
		{
			size_t index = Num_entities;
			Num_entities += 1;

			for (const auto& [k, v] : m_cmans)
				v->Add_entities(1);

			return index;
		}

		template<Component_maybe_const C>
		static Component_type_id Get_id()
		{
			return std::type_index{ typeid(C) };
		}

		template <Component_maybe_const Comp>
		bool Has_component_man() const
		{
			return m_cmans.contains(Get_id<std::remove_const_t<Comp>>());
		}

		template <Component Comp> Component_man<Comp>&
			Get_or_create_component_man()
		{
			auto id = Get_id<Comp>();

			auto found = m_cmans.find(id);
			if (found != m_cmans.end())
				return static_cast<Component_man<Comp>&>(*found->second);

			const auto [it, success] = m_cmans.emplace(id, std::make_unique<Component_man<Comp>>());

			if (!success) throw new std::exception(); // impossible!

			auto& cman = static_cast<Component_man<Comp>&>(*it->second);
			cman.Add_entities(Num_entities);

			return cman;
		}

		template <Component_const Comp>
		const Component_man<std::remove_const_t<Comp>> &
			Get_or_create_component_man()
		{
			return Get_or_create_component_man<std::remove_const_t<Comp>>();
		}

		template <Component Comp> Component_man<Comp>*
			Try_get_component_man()
		{
			auto id = Get_id<Comp>();
			auto found = m_cmans.find(id);
			if (found != m_cmans.end())
				return static_cast<Component_man<Comp>*>(found->second.get());
			else
				return nullptr;
		}

		template <Component_const Comp>
		const Component_man<std::remove_const_t<Comp>>*
			Try_get_component_man()
		{
			return Try_get_component_man<std::remove_const_t<Comp>>();
		}

		template <Component C>
		struct Const_component_filler
		{
			const Component_man<C>* m_comp_man;
			Const_component_filler(const Component_man<C>* comp_man) : m_comp_man{ comp_man } {}
			bool Can_ever_fill()	{ return m_comp_man != nullptr; }
			bool Can_fill(size_t i) { return m_comp_man->Exists(i); }
			const C& Fill(size_t i) { return *m_comp_man->Try_get(i); }
		};

		template <Component C>
		struct Mutable_component_filler
		{
			Component_man<C>* m_comp_man;
			Mutable_component_filler(Component_man<C>* comp_man) : m_comp_man{ comp_man } {}
			bool Can_ever_fill()	{ return m_comp_man != nullptr; }
			bool Can_fill(size_t i) { return m_comp_man->Exists(i); }
			C & Fill(size_t i)		{ return *m_comp_man->Try_get(i); }
		};

		template <Component C>
		struct Not_component_filler
		{
			const Component_man<C>* m_comp_man;
			Not_component_filler(const Component_man<C>* comp_man) : m_comp_man{ comp_man } {}
			bool Can_ever_fill()	{ return true; }
			bool Can_fill(size_t i)	{ return !m_comp_man->Exists(i); }
			Not<C> Fill(size_t i)	{ return Not<C>{}; }
		};

		template <Component C>
		struct Maybe_of_const_component_filler
		{
			const Component_man<C>* m_comp_man;
			Maybe_of_const_component_filler(const Component_man<C>* comp_man) : m_comp_man{ comp_man } {}
			bool Can_ever_fill() { return true; }
			bool Can_fill(size_t i) { return true; }
			Maybe<const C> Fill(size_t i)
			{
				if (m_comp_man == nullptr)
					return Maybe<const C>{nullptr};
				else
					return Maybe<const C>{m_comp_man->Try_get(i)};
			}
		};

		template <Component C>
		struct Maybe_of_mutable_component_filler
		{
			Component_man<C>* m_comp_man;
			Maybe_of_mutable_component_filler(Component_man<C>* comp_man) : m_comp_man{ comp_man } {}
			bool Can_ever_fill() { return true; }
			bool Can_fill(size_t i) { return true; }
			Maybe<C> Fill(size_t i)
			{
				if (m_comp_man == nullptr)
					return Maybe<C>{nullptr};
				else
					return Maybe<C>{m_comp_man->Try_get(i)};
			}
		};

		template <class Arg>
		auto Get_arg_filler();

		template <Component_const_ref Arg>
		auto Get_arg_filler()
		{
			using Comp = std::decay_t<Arg>;
			auto* comp_man = Try_get_component_man<Comp>();
			return Const_component_filler<Comp>(comp_man);
		}

		template <Component_ref Arg>
		auto Get_arg_filler()
		{
			using Comp = std::decay_t<Arg>;
			auto* comp_man = Try_get_component_man<Comp>();
			return Mutable_component_filler<Comp>(comp_man);
		}

		template <Not_decayed Arg>
		auto Get_arg_filler()
		{
			using Comp = std::decay_t<Arg>::Underlying_component;
			auto* comp_man = Try_get_component_man<const Comp>();
			return Not_component_filler<Comp>(comp_man);
		}

		template <Maybe_of_const_component Arg>
		auto Get_arg_filler()
		{
			using Comp = std::decay_t<Arg>::Underlying_component;
			auto* comp_man = Try_get_component_man<const Comp>();
			return Maybe_of_const_component_filler<Comp>(comp_man);
		}

		template <Maybe_of_mutable_component Arg>
		auto Get_arg_filler()
		{
			using Comp = std::decay_t<Arg>::Underlying_component;
			auto* comp_man = Try_get_component_man<Comp>();
			return Maybe_of_mutable_component_filler<Comp>(comp_man);
		}

	public:

		template <Component ... Components>
		requires (hopu::Distinct_v<Components...>)
			void Add_entity(std::invocable<Components&...> auto&& f)
		{
			// 1) add an entity
			// 2) get or create component managers
			// 3) create components
			// 4) call f with the components

			// 1) add an entity
			size_t new_entity_index = Add_entity_private();

			[&f, new_entity_index](auto & ... component_managers)
			{
				// 3) create components
				if (!(component_managers.Try_create(new_entity_index) && ... && true))
					throw std::exception(); // impossible

				// 4) call f with the components
				f(*component_managers.Try_get(new_entity_index) ...);
			}
				// 2) get or create component managers
			(Get_or_create_component_man<Components>() ...);
		}

		template<class ... Args>
		requires Valid_argument_list<Args...>
			void Loop_all(std::invocable<Args...> auto&& f)
		{
			// 1) get all argument fillers
			// 2) return early if one or more arguments can never be filled
			// 3) loop all entities
			// 4) if entity has all components
			// 5) call f

			return[&f, n = Num_entities](auto ... fillers)
			{
				// 2) return early if one or more arguments can never be filled
				if (!(fillers.Can_ever_fill() && ...))
					return;

				// 3) loop all entities
				for (size_t i = 0; i < n; i += 1)
				{
					// 4) if entity has all components
					if ((fillers.Can_fill(i) && ...))
					{
						// 5) call f
						f(fillers.Fill(i) ...);
					}
				}
			}
				// 1) get all component managers
			(Get_arg_filler<Args>() ...);
		}
	};
}
